# Set up the system, user profile, and related variables.
# /etc/profile will be sourced by bash automatically
# Set up the home environment profile.
if [ -f ~/.profile ]; then source ~/.profile; fi

# Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then source ~/.bashrc; fi

PS1='\u@\h \w$GUIX_ENVIRONMENT:+ [env]
     \$ '


# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

#
# Guix
#


# Add extra-profile
# from https://guix.gnu.org/cookbook/en/html_node/Basic-setup-with-manifests.html
export GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
# all
for i in $GUIX_EXTRA_PROFILES/*; do
  profile=$i/$(basename "$i")
  if [ -f "$profile"/etc/profile ]; then
    GUIX_PROFILE="$profile"
    . "$GUIX_PROFILE"/etc/profile
  fi
  unset profile
done

## subset
#for i in "$GUIX_EXTRA_PROFILES"/doom-emacs "$GUIX_EXTRA_PROFILES"/latex "$GUIX_EXTRA_PROFILES"/julia "$GUIX_EXTRA_PROFILES"/emacs "$GUIX_EXTRA_PROFILES"/R "$GUIX_EXTRA_PROFILES"/python; do
#  profile=$i/$(basename "$i")
#  if [ -f "$profile"/etc/profile ]; then
#    GUIX_PROFILE="$profile"
#    . "$GUIX_PROFILE"/etc/profile
#  fi
#  unset profile
#done

# Set default user profile for guix
GUIX_PROFILE="$HOME/.guix-profile"
. "$GUIX_PROFILE/etc/profile"

# Set compiler
export CC=gcc
if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    export MOZ_ENABLE_WAYLAND=1
fi


# Wayland compatibility
if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    export MOZ_ENABLE_WAYLAND=1
fi


#
# Nix
#
#export FONTCONFIG_FILE=${pkgs.fontconfig.out}/etc/fonts/fonts.conf
export FONTCONFIG_PATH=$HOME/.guix-profile/etc/fonts
export NIXPKGS_ALLOW_UNFREE=1
XDG_DATA_DIRS="$HOME/.nix-profile/share:$XDG_DATA_DIRS"

# set PATH so it includes nix bin if it exists
if [ -d "$HOME/.nix-profile/bin" ] ; then
    PATH="$HOME/.nix-profile/bin:$PATH"
fi

PATH=$HOME/.nix-profile/bin:$PATH

# Rstudio
# temp fix for white empty panel
RSTUDIO_CHROMIUM_ARGUMENTS="--no-sandbox"

# Flatpak ####
alias zoom="flatpak run us.zoom.Zoom"
XDG_DATA_DIRS="$HOME/.local/share/flatpak/exports/share:$XDG_DATA_DIRS"

#
# Doom emacs
#
export EMACS=~/.guix-profile/bin/emacs-28.2
export EMACSLOADPATH=~/.guix-profile/share/emacs/site-lisp
PATH="$HOME/.config/emacs/bin:$PATH"


#
# Julia
#
#export JULIA_DEPOT_PATH="$HOME/.julia"
# set initial number of threads
export JULIA_NUM_THREADS=6

;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
	     (guix channels)
	     (guix sets)

	     ;; Packages
             (gnu packages xdisorg)
	     (gnu packages dunst)

             (gnu services shepherd)

	     ;; Services
	     (gnu home services shells)
	     (gnu home services desktop)
	     (gnu home services shepherd)
	     (gnu home services pm)
	     (gnu home services guix)

	     (dwl-guile home-service)
	     (dwl-guile patches)
	     (dtao-guile home-service)
	     )


(define %tags-and-layout
  (append
   (map
    (lambda (tag)
      (let ((str (string-append "^p(8)" (number->string tag) "^p(8)"))
            (index (- tag 1)))
	(dtao-block
	 (interval 0)
	 (events? #t)
	 (click `(match button
                   (0 (dtao:view ,index))))
	 (render `(cond
                   ((dtao:selected-tag? ,index)
                    ,(string-append "^bg(#ffcc00)^fg(#191919)" str "^fg()^bg()"))
                   ((dtao:urgent-tag? ,index)
                    ,(string-append "^bg(#ff0000)^fg(#ffffff)" str "^fg()^bg()"))
                   ((dtao:active-tag? ,index)
                    ,(string-append "^bg(#323232)^fg(#ffffff)" str "^fg()^bg()"))
                   (else ,str))))))
    (iota 9 1))
   (list
    (dtao-block
     (events? #t)
     (click `(dtao:next-layout))
     (render `(string-append "^p(4)" (dtao:get-layout))))

    )))


(define my-home-dtao-guile-configuration
  (home-dtao-guile-configuration
   (auto-start? #t)
   (config
    (dtao-config
     ;; A font string in fcft format.
     (font "DejaVu Sans Mono:size=16")
     ;; Read `root', `border' and `text' colors from dwl-guile.
     ;;(use-dwl-guile-colorscheme? #t)
     (background-color "28282866")
     (border-color "333333FF")
     (foreground-color "ebdbb2")
     (padding-left 8)
     (padding-right 8)
     (padding-top 2)
     (padding-bottom 2)
     ;; Request an exclusive zone for the bar to prevent overlapping.
     (exclusive? #t)
     ;; Layer to render the bar in (LAYER-TOP, LAYER-BOTTOM, LAYER-OVERLAY, LAYER-BACKGROUND).
     (layer 'LAYER-BOTTOM)
     ;; Render the bar at the bottom of the screen.
     (bottom? #f)
     ;; Height of the bar in pixels. Set to #f for automatic height based on font size.
     (height #f)
     ;; Delimiter string of arbitrary length inserted between blocks.
     (delimiter #f)
     ;; Additional spacing on each side of the delimiter string.
     (block-spacing 0)
     (left-blocks %tags-and-layout)
     (center-blocks (list
		     (dtao-block
		      (events? #t) ;; Must be enabled to correctly re-render upon event/state change
		      (render `(dtao:title)))
		     ))
     (right-blocks (list
		    (dtao-block
		     (interval 1)
		     (render `(strftime "%Y-%m-%d %H:%M " (localtime (current-time)))))
		    ))
     ;; List of Guile module dependencies needed to run your blocks.
     (modules '()) 
     ))
   ))

(define my-home-dwl-guile-configuration
  (home-dwl-guile-configuration
   (startup-command "foot --server <&-")
   (native-qt? #t)
   (auto-start? #t)
   (reload-config-on-change? #t)
   (config
    '((

       ;; Remove default config
       (setq inhibit-defaults? #f)
       ;; Touchpad
       (setq tap-to-click? #t)
       ;;   ;; Keyboard config
       (setq repeat-rate 50)
       (setq repeat-delay 300)
       (set-xkb-rules '((model . "pc105")
			(layout . "fr")
			(rules . "evdev")
			(options . "ctrl:nocaps")))
       ;; Esthetics
       (setq gaps-ih 5)
       (setq border-px 3)
       ;; Start repl for interactive during runtime through emacs geiser
       (dwl:start-repl-server)

       ;; autostart ;; tmp
       (dwl:spawn "swaybg" "-m" "fill" "-i" "/home/k8x1d/Pictures/wallpapers/paintings/Ryck_Rudd/Study_for_Portrait_of_Egon_Schiele.jpg")
       ;; Keys
       (set-keys
	;; Menu launcher
	"s-d" '(dwl:spawn "bemenu-run" "--fn" "DejaVu Sans Mono 16")
	;; Terminal
	"s-<return>" '(dwl:spawn "foot")
	"s-j" '(dwl:focus-stack 1)
	"s-k" '(dwl:focus-stack -1)
	"s-l" '(dwl:change-master-factor 0.05)
	"s-h" '(dwl:change-master-factor -0.05)
	"s-<page-up>" '(dwl:change-masters 1)
	"s-<page-down>" '(dwl:change-masters -1)
	"s-t" '(dwl:cycle-layout 1)
	"s-<left>" '(dwl:focus-monitor 'DIRECTION-LEFT)
	"s-<right>" '(dwl:focus-monitor 'DIRECTION-RIGHT)
	"s-<up>" '(dwl:focus-monitor 'DIRECTION-UP)
	"s-<down>" '(dwl:focus-monitor 'DIRECTION-DOWN)
	"s-S-<left>" '(dwl:tag-monitor 'DIRECTION-LEFT)
	"s-S-<right>" '(dwl:tag-monitor 'DIRECTION-RIGHT)
	"s-S-<up>" '(dwl:tag-monitor 'DIRECTION-UP)
	"s-S-<down>" '(dwl:tag-monitor 'DIRECTION-DOWN)
	"s-q" 'dwl:kill-client
	"s-<space>" 'dwl:zoom
	"s-<tab>" 'dwl:view
	"s-S-0" '(dwl:view 0) ;; 0 will show all tags
	"s-f" 'dwl:toggle-fullscreen
	"S-s-<space>" 'dwl:toggle-floating
	"S-s-<escape>" 'dwl:quit
	"<XF86PowerOff>" 'dwl:quit
	"s-<mouse-left>" 'dwl:move
	"s-<mouse-middle>" 'dwl:toggle-floating
	"s-<mouse-right>" 'dwl:resize
	;; Multimedia keys
	"<XF86MonBrightnessUp>" '(dwl:spawn "light" "-A" "5")
	"<XF86MonBrightnessDown>" '(dwl:spawn "light" "-U" "5")
	"<XF86AudioRaiseVolume>" '(dwl:spawn  "pactl" "set-sink-volume" "@DEFAULT_SINK@" "+5%")
	"<XF86AudioLowerVolume>" '(dwl:spawn "pactl" "set-sink-volume" "@DEFAULT_SINK@" "-5%")
	"<XF86AudioMute>" '(dwl:spawn "pactl" "set-sink-mute" "@DEFAULT_SINK@" "toggle")
	"<XF86AudioMicMute>" '(dwl:spawn "pactl" "set-source-mute" "@DEFAULT_SOURCE@" "toggle")
	)
       )))
   ))

(define my-channels
  (list
  ;; (channel
  ;;  (name 'k8x1d)
  ;;  (url "https://gitlab.com/frame14/guix-channel.git")
  ;;  (branch "master")
  ;;  (commit "332d404f8ee53a2f0fc4c9a7ddd3d9ea9c0eea1f")
  ;;  (introduction
  ;;   (make-channel-introduction
  ;;    "332d404f8ee53a2f0fc4c9a7ddd3d9ea9c0eea1f"
  ;;    (openpgp-fingerprint
  ;;     "A180 8C8D E727 2D87 15CD  AB96 39AA 7B97 9BCC 55C5"))))


   ;;(channel
   ;; (name 'emacs)
   ;; (url "https://github.com/babariviere/guix-emacs")
   ;; (branch "master")
   ;; (commit "057fe38efb4b5f8776d50b10b33e963dc52b8dd8")
   ;; (introduction
   ;;  (make-channel-introduction
   ;;   "72ca4ef5b572fea10a4589c37264fa35d4564783"
   ;;   (openpgp-fingerprint
   ;;    "261C A284 3452 FB01 F6DF  6CF4 F9B7 864F 2AB4 6F18"))))
   (channel
    (name 'guix-hpc)
    (url "https://gitlab.inria.fr/guix-hpc/guix-hpc")
    (branch "master")
    (commit "aa6e4f42d56269533331d1dd07a812a0abfd8523")
    )
   (channel
    (name 'guix-cran)
    (url "https://github.com/guix-science/guix-cran")
    (branch "master")
    (commit "4d8b3b60b593f9bebc0742f5f67c354869dc4e49")
    )
   ;; Guile scheme configurable version of dtao status bar  
   (channel
    (name 'home-service-dtao-guile)
    (url "https://github.com/engstrand-config/home-service-dtao-guile")
    (branch "main")
    (commit "17d00036349ee9eb57486d47502c2f56ce0b8844")
    (introduction
     (make-channel-introduction
      "17d00036349ee9eb57486d47502c2f56ce0b8844"
      (openpgp-fingerprint
       "C9BE B8A0 4458 FDDF 1268 1B39 029D 8EB7 7E18 D68C"))))

   ;; Guile scheme configurable version of dwl window manager  
   (channel
    (name 'home-service-dwl-guile)
    (url "https://github.com/engstrand-config/home-service-dwl-guile")
    (branch "main")
    (commit "97bb8df1bba8bdafb0b62452a89ef9f92d66c8f0")
    (introduction
     (make-channel-introduction
      "97bb8df1bba8bdafb0b62452a89ef9f92d66c8f0"
      (openpgp-fingerprint
       "C9BE B8A0 4458 FDDF 1268 1B39 029D 8EB7 7E18 D68C"))))
   ;; Give access to some useful scientific programs, e.g. rstudio
   (channel
    (name 'guix-science)
    (url "https://github.com/guix-science/guix-science.git")
    (branch "master")
    (commit "11222bb675c89855ad24c0305e3f2736f4ef3a1e")
    (introduction
     (make-channel-introduction
      "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
      (openpgp-fingerprint
       "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
   ;; Access to nongnu packages ;; necessery for laptop wifi and cpu updates
   (channel
    (name 'nonguix)
    (url "https://gitlab.com/nonguix/nonguix")
    (branch "master")
    (commit "026b5b5c47f606161113398cda710a7c85164937")
    (introduction
     (make-channel-introduction
      "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
      (openpgp-fingerprint
       "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
   ;; Defaults guix channel
   (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (branch "master")
    (commit "2d06dfc050114dba44e791d8decc8eaa705fee01")
    (introduction
     (make-channel-introduction
      "9edb3f66fd807b096b48283debdcddccfea34bad"
      (openpgp-fingerprint
       "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
   ))




(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
 ;; Packages
 (packages (specifications->packages (list


				      ;; Test
				      "x86-energy-perf-policy"

                                      ;;"batsignal"
                                      ;;"plasma"

				      ;; Packages management
				      "flatpak"

				      ;; Browser
                                      "firefox"
				      "icecat"
				      "nyxt"

				      ;; Fonts
                                      "font-gnu-unifont:bin"
                                      "font-fira-mono"
                                      "font-jetbrains-mono"
                                      "font-fira-code"
                                      "font-fira-sans"
                                      "font-iosevka"
                                      "font-iosevka-aile"
                                      "font-iosevka-term"
                                      "font-awesome"
                                      "font-dejavu"
				      "font-hack"
                                      "fontconfig"

				      ;; Multimedia
                                      "mpd-mpc"
                                      "ncmpcpp"
                                      "vlc"
                                      "imagemagick"
                                      "mpd"
                                      "mpv"
                                      "cmus"
				      "ffmpeg"
                                      "yt-dlp"
                                      "imv"

				      ;; Utilities
                                      "docker-cli"
                                      "powertop"
                                      "password-store"
                                      "pass-otp"
                                      "gnupg"
                                      "p7zip"
                                      "rsync"
				      "nextcloud-client"
                                      "unzip"
                                      "htop"
                                      "git"
				      "sed"
				      "cpupower"
				      "docker"
				      "sddm"
                                      "tlp"

				      ;; Dependencies
                                      "node"
                                      "perl"
                                      "subversion"
                                      "curl"
                                      "libtool"
                                      "python-pylint"
                                      "openjdk"

				      ;; Desktop ;;
                                      "xdg-utils"
                                      "adwaita-icon-theme"
                                      "playerctl"
                                      "light"
                                      "gnome-shell-extension-vertical-overview"
                                      "gnome-shell-extension-dash-to-dock"
                                      "gnome-shell-extensions"
                                      "libreoffice"
                                      "signal-desktop"
                                      "lxappearance"
                                      "pulseaudio"
                                      "pavucontrol"
                                      "gnome-tweaks"
                                      "alacritty"
                                      "gimp"
                                      "bluez"
                                      "protonvpn-cli"
                                      "okular"
				      "libnotify"
				      "dunst"
                                      "icedove"

				      ;; Sound
				      "pipewire"
				      "wireplumber"

				      ;; Xorg
                                      "xlsclients"
                                      "xclip"
                                      "picom"
                                      "polybar"
                                      "feh"
                                      "scrot"

				      ;; Wayland
                                      "wl-clipboard"
				      "qtwayland" 
                                      "foot"
                                      "waybar"
				      "bemenu"
                                      "swaybg"
				      "grimshot"
				      "swayidle"
				      ;;


				      ;; Building tool
                                      "rust-cargo"
                                      "gcc-toolchain"
                                      "python-pip"
                                      "shellcheck"
                                      "python"
                                      "make"
                                      "cmake"
                                      ;;"neovim" ;;
				      )))

 ;; Below is the list of Home services.  To search for available
 ;; services, run 'guix home search KEYWORD' in a terminal.
 (services (list
            (service home-bash-service-type
                     (home-bash-configuration
                       (guix-defaults? #f)
                      (aliases '(("grep" . "grep --color=auto") ("ll" . "ls -l")
                                 ("ls" . "ls -p --color=auto")))
                      (bashrc (list (local-file
                                     "/home/k8x1d/src/guix-config/.bashrc"
                                     "bashrc")))
                      (bash-profile (list (local-file
                                           "/home/k8x1d/src/guix-config/.bash_profile"
                                           "bash_profile")))
;;                      	      (bash-profile (list (plain-file "bash-profile"
;;                                                              "export HISTFILE=$XDG_CACHE_HOME/.bash_history
;;                                                              # Nix
;;                                                              export NIXPKGS_ALLOW_UNFREE=1
;;                                                              PATH=$HOME/.nix-profile/bin:$PATH
;;                                                              XDG_DATA_DIRS=$HOME/.nix-profile/share:$XDG_DATA_DIRS
;;                                                              FONTCONFIG_PATH=$HOME/.guix-profile/etc/fonts
;;                                                              RSTUDIO_CHROMIUM_ARGUMENTS=--no-sandbox
;;
;;export GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
;;for i in \"$GUIX_EXTRA_PROFILES\"/doom-emacs \"$GUIX_EXTRA_PROFILES\"/latex \"$GUIX_EXTRA_PROFILES\"/julia \"$GUIX_EXTRA_PROFILES\"/emacs \"$GUIX_EXTRA_PROFILES\"/R \"$GUIX_EXTRA_PROFILES\"/python; do
;;  profile=$i/$(basename \"$i\")
;;  if [ -f \"$profile\"/etc/profile ]; then
;;    GUIX_PROFILE=\"$profile\"
;;    . \"$GUIX_PROFILE\"/etc/profile
;;  fi
;;  unset profile
;;done
;;
;;"
;; )))

                      ))
	    (service home-shepherd-service-type
		     (home-shepherd-configuration
		      (services (list
				 (shepherd-service
				  (documentation "Notification daemon.")
				  (provision '(dunst))
				  (start #~(make-forkexec-constructor
					    (list #$(file-append dunst "/bin/dunst"))
					    ))
				  (stop #~(make-kill-destructor)))

				 (shepherd-service
				  (documentation "Cloud service.")
				  (provision '(dropbox))
				  (start #~(make-forkexec-constructor 
					  '("dropbox")))
				 (stop #~(make-kill-destructor)))
				 ))
		      ))

	    ;; Desktop services
	    (service home-redshift-service-type
		     (home-redshift-configuration
		      (redshift redshift-wayland)
		      (location-provider 'manual)
		      (daytime-temperature 6500)
		      (nighttime-temperature 3000)
		      (latitude 45.50884)    
		      (longitude -73.58781)))
	    (service home-dbus-service-type)
	    ;; Power management
	    (service home-batsignal-service-type)
	    ;; (service home-batsignal-service-type
	    ;;	      (home-batsignal-configuration
	    ;;	       (warning-message #t)
	    ;;	       (critical-message #t)
	    ;;	       (batteries '("BAT1"))
	    ;;	       ))
	    ;; Guix 
	    (service home-channels-service-type my-channels)

	   ;; ;; Wm set-up ;; work in progress
	   ;; (service home-dtao-guile-service-type my-home-dtao-guile-configuration)
	   ;; (service home-dwl-guile-service-type my-home-dwl-guile-configuration)
      ))
 )
